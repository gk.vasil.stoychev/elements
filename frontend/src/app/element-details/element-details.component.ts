import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ElementsService} from '../services/elements/elements.service';
import {NgForm} from '@angular/forms';
import {SessionService} from '../session/state/session.service';
import {ElementDataDto} from '../services/shared/model/element.data.dto';
import {AppPaths} from '../shared/app.paths';

@Component({
  selector: 'app-element-details',
  templateUrl: './element-details.component.html',
  styleUrls: ['./element-details.component.css']
})
export class ElementDetailsComponent implements OnInit {

  private elementAtomicNumber: number = this.activatedRoute.snapshot.params.id;
  element: ElementDataDto;
  showError = false;

  constructor(private activatedRoute: ActivatedRoute, private elementsService: ElementsService,
              private sessionService: SessionService, private router: Router) { }

  ngOnInit(): void {
    this.getComponentDetails();
  }

  getComponentDetails(): void {
    this.elementsService.getDetails(this.elementAtomicNumber)
      .subscribe(element => {
          this.element = element;
        }
      );
  }

  updateElement(form: NgForm): void {
    if (!form.valid) {
      return;
    }
    const jwt = this.sessionService.getRawToken();
    if (jwt === null || jwt === '' || jwt === undefined || !this.sessionService.getUserIsAdmin()) {
      window.alert('You need to be logged in as an administrator to do this.');
      return;
    }
    const element: ElementDataDto = form.value;
    element.alternative_names = 'n/a';
    element.discovery_and_first_isolation = 'n/a';
    this.elementsService.updateElement(element).subscribe();
  }

  goToAuthentication(): void {
    this.router.navigate([AppPaths.home]);
  }
}
