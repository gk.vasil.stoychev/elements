import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RegisterComponent} from './register/register.component';
import {AuthenticateComponent} from './authenticate/authenticate.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {UpperToolbarComponent} from './upper-toolbar/upper-toolbar.component';
import { AllElementsComponent } from './all-elements/all-elements.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';
import {environment} from '../environments/environment';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatTabsModule} from '@angular/material/tabs';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatListModule} from '@angular/material/list';
import { ElementDetailsComponent } from './element-details/element-details.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    AuthenticateComponent,
    PageNotFoundComponent,
    UpperToolbarComponent,
    AllElementsComponent,
    ElementDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    MatToolbarModule,
    MatButtonModule,
    FontAwesomeModule,
    MatTabsModule,
    MatTooltipModule,
    MatGridListModule,
    MatMenuModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
