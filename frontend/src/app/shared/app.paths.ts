export const AppPaths = {
  home: '/',
  register: '/register',
  allElements: '/all-elements',
  elementDetails: '/element-details'
};
