import { Component, OnInit } from '@angular/core';
import {faSignOutAlt, faSignInAlt, faChevronCircleDown} from '@fortawesome/free-solid-svg-icons';
import {ToolbarService} from '../services/toolbar/toolbar.service';
import {SessionService} from '../session/state/session.service';
import {Router} from '@angular/router';
import {AppPaths} from '../shared/app.paths';
import {AuthenticationService} from '../services/authentication/authentication.service';

@Component({
  selector: 'app-upper-toolbar',
  templateUrl: './upper-toolbar.component.html',
  styleUrls: ['./upper-toolbar.component.css']
})
export class UpperToolbarComponent implements OnInit {

  faCircleDown = faChevronCircleDown;
  faSignOut = faSignOutAlt;
  faSignIn = faSignInAlt;
  isAdmin = false;

  constructor(public toolbarService: ToolbarService, private sessionService: SessionService, private router: Router,
              private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.authenticationService.loggingNotifier.subscribe(() => {
      this.isAdmin = this.sessionService.getUserIsAdmin();
    });
  }

  signOut(): void {
    this.sessionService.fullSessionClear();
    this.router.navigate([AppPaths.home]);
  }

  goToAllElements(): void {
    this.router.navigate([AppPaths.allElements]);
  }

}
