import { Component, OnInit } from '@angular/core';
import {ElementsService} from '../services/elements/elements.service';
import {SimpleElementDto} from '../services/shared/model/simple.element.dto';
import {Router} from '@angular/router';
import {AppPaths} from '../shared/app.paths';
import {NgForm} from '@angular/forms';
import {GroupPeriodFilter} from '../services/shared/model/group.period.filter';
import {ElementDataDto} from '../services/shared/model/element.data.dto';

@Component({
  selector: 'app-all-elements',
  templateUrl: './all-elements.component.html',
  styleUrls: ['./all-elements.component.css']
})
export class AllElementsComponent implements OnInit {

  elements: SimpleElementDto[] = [];
  groupAndPeriodFilter: GroupPeriodFilter = {};
  showFilterError = false;

  constructor(private elementsService: ElementsService, private router: Router) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.elementsService.getAll().subscribe(e =>
      this.elements = e
    );
  }

  goToDetails(atomicNumber: number): void {
    this.router.navigate([`${AppPaths.elementDetails}/${atomicNumber}`]);
  }

  getFiltered(form: NgForm): void {
    this.showFilterError = false;
    const group: number = form.value.group;
    const period: number = form.value.period;
    if (group === undefined && period === undefined) {
      this.showFilterError = true;
    }
    this.elementsService.getFiltered(group, period)
      .subscribe(r => {
        if (r.length > 0) {
          this.elements = [];
          r.forEach((x: ElementDataDto) =>
          {
            const item: SimpleElementDto = {
              atomicNumber: x.atomic_number,
              name: x.name
            }
            this.elements.push(item);
          });
        }
       },
      () => {
        this.showFilterError = true;
      });
  }
}
