import { Injectable } from '@angular/core';
import { SessionStore } from './session.store';
import { SessionQuery } from './session.query';

@Injectable({ providedIn: 'root' })
export class SessionService {

  constructor(private sessionStore: SessionStore, private query: SessionQuery) {
  }

  createUserStore(userId: number, token: string, username: string, userIsAdmin: boolean): void {
    this.sessionStore.update({userId});
    this.sessionStore.update({token});
    this.sessionStore.update({username});
    this.sessionStore.update({userIsAdmin});
  }

  updateUserId(userId: number): void {
    this.sessionStore.update({userId});
  }

  getRawUserId(): number {
    return this.query.getValue().userId;
  }

  getRawToken(): string {
    return this.query.getValue().token;
  }

  getUsername(): string {
    return this.query.getValue().username;
  }

  getUserIsAdmin(): boolean {
    return this.query.getValue().userIsAdmin;
  }

  fullSessionClear(): void {
    this.sessionStore.update( {
      token: null,
      userId: null,
      username: null
    });
  }
}
