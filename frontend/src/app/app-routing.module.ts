import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthenticateComponent} from './authenticate/authenticate.component';
import {RegisterComponent} from './register/register.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AllElementsComponent} from './all-elements/all-elements.component';
import {ElementDetailsComponent} from './element-details/element-details.component';

const routes: Routes = [
  { path: '', component: AuthenticateComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'all-elements', component: AllElementsComponent },
  { path: 'element-details/:id', component: ElementDetailsComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
