import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SimpleElementDto} from '../shared/model/simple.element.dto';
import {ElementDataDto} from '../shared/model/element.data.dto';
import {SessionService} from '../../session/state/session.service';

@Injectable({
  providedIn: 'root'
})
export class ElementsService {

  private ENDPOINT = `${environment.apiUrl}/elements`;

  constructor(private httpClient: HttpClient, private sessionService: SessionService) { }

  private static prepareHeaders(token: string): HttpHeaders {
    return new HttpHeaders({
      Authorization: `${token}`
    });
  }

  public getAll(): Observable<SimpleElementDto[]> {
    return this.httpClient.get<SimpleElementDto[]>(`${this.ENDPOINT}/`);
  }

  public getDetails(atomicNumber: number): Observable<ElementDataDto> {
    const params = new HttpParams().append('atomicNumber', `${atomicNumber}`);
    return this.httpClient.get<ElementDataDto>(`${this.ENDPOINT}/details`, {params});
  }

  public getFiltered(group?: number, period?: number): Observable<any> {
    let params = new HttpParams();
    if (group !== undefined && group !== null) {
      params = params.append('group', `${group}`);
    } else {
      params = params.delete('group');
    }
    if (period !== undefined && period !== null) {
      params = params.append('period', `${period}`);
    } else {
      params = params.delete('period');
    }
    return this.httpClient.get<ElementDataDto>(`${this.ENDPOINT}/filtered`, {params});
  }

  public updateElement(element: ElementDataDto): Observable<void> {
    const options = {headers: ElementsService.prepareHeaders(this.sessionService.getRawToken())};
    return this.httpClient.post<void>(`${this.ENDPOINT}/update`, element, options);
  }
}
