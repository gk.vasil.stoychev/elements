export interface GroupPeriodFilter {
  group?: number;
  period?: number;
}
