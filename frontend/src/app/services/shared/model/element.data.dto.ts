export interface ElementDataDto {
  atomic_number: number;
  name: string;
  alternative_name: string;
  alternative_names?: string;
  symbol: string;
  appearance: string;
  discovery: string[];
  discovery_and_first_isolation?: string;
  discoveryYear: string;
  group_block: number;
  period: number;
}
