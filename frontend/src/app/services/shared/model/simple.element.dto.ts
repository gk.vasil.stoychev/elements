export interface SimpleElementDto {
  atomicNumber: number;
  name: string;
}
