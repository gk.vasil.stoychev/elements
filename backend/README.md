# Getting Started

##[Test the application here!](https://https://vast.rest/all-elements/)

### 1. How to test on *Production*:
* You will be sent to the "All Elements Page"
* You can filter by Group and Period
* You can get the element details by selecting an Element
* You can only edit elements as a registered administrator
* You can navigate to the login from the menu next to "Authentication"
* You can register a user at https://vast.rest/register
* You will have access only to standard users
* You can also login https://vast.rest/ as an admin with user: admin, pass: admin
* You can also get direct api results - for example https://vast.rest/api/elements/
* There's also a folder in the project called "example_postman_requests", which can be
imported to Postman

### 2. How to test on *Locally*:
* You can do everything as in Production, with two additions:
* You can register a user with the name "admin" and any password
* or You can create a normal user and edit the SQL table **authority** and give the user 
"ADMIN_ROLE"

### 3. For a local setup in IntelliJ/WebStorm:
* Should be a straight forward build for both BE and FE
* Have a running MySQL + with the correct user/password (as given in application.properties file)
* Tables should be created automatically

### 4. Others:
* There are example unit tests for the Elements Service/Controller
* There is only sample logging
* Server used for deployment, reverse proxy config and (free) SSL certificate generation - Caddy 2
* The project was deployed using Docker + Docker Compose on a VPS from Contabo
