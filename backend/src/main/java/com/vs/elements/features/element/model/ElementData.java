package com.vs.elements.features.element.model;

import com.vs.elements.features.element.dto.ElementDataDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "element_data")
public class ElementData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int atomicNumber;
    private String name;
    private String alternativeName;
    private String atomicSymbol;
    private String appearance;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "element_data_id")
    private List<Discoverer> discoverer;
    private String discoveryYear;
    private int groupBlock;
    private int period;

    public ElementData() {

    }

    public void setElementDataFromDto(ElementDataDto elementDataDto) {
        this.atomicNumber = elementDataDto.getAtomicNumber();
        this.name = elementDataDto.getName();
        this.alternativeName = elementDataDto.getAlternativeName();
        this.atomicSymbol = elementDataDto.getAtomicSymbol();
        this.appearance = elementDataDto.getAppearance();
        this.discoveryYear = elementDataDto.getDiscoveryYear();
        this.groupBlock = elementDataDto.getGroup();
        this.period = elementDataDto.getPeriod();
    }
}
