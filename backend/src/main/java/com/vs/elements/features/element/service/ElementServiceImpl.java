package com.vs.elements.features.element.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vs.elements.features.element.dto.ElementDataDto;
import com.vs.elements.features.element.dto.SimpleElementDto;
import com.vs.elements.features.element.exception.MissingIdentifierException;
import com.vs.elements.features.element.exception.UnidentifiedElementException;
import com.vs.elements.features.element.model.Discoverer;
import com.vs.elements.features.element.model.ElementData;
import com.vs.elements.features.element.repository.DiscovererRepository;
import com.vs.elements.features.element.repository.ElementDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ElementServiceImpl implements ElementService {

    private static final int NUMBER_OF_ELEMENTS = 117;

    private final ObjectMapper objectMapper;
    private final ElementDataRepository elementDataRepository;
    private final DiscovererRepository discovererRepository;

    @Autowired
    public ElementServiceImpl(ObjectMapper objectMapper,
                              ElementDataRepository elementDataRepository,
                              DiscovererRepository discovererRepository) {
        this.objectMapper = objectMapper;
        this.elementDataRepository = elementDataRepository;
        this.discovererRepository = discovererRepository;
    }

    @Override
    public void loadDataToDb() throws IOException {
        if (elementDataRepository.count() >= NUMBER_OF_ELEMENTS) {
            return;
        }

        Resource resource = getData(ElementDataDto.class.getClassLoader());

        ElementDataDto[] elementDataDto = objectMapper.readValue(resource.getInputStream(), ElementDataDto[].class);
        for (ElementDataDto dto : elementDataDto) {
            ElementData element = new ElementData();
            element.setElementDataFromDto(dto);
            int id = elementDataRepository.save(element).getId();
            List<Discoverer> discoverers = mapDiscoverers(dto.getDiscoverers(), id);
            discovererRepository.saveAll(discoverers);
        }
    }

    private List<Discoverer> mapDiscoverers(String[] discovererNames, int elementId) {
        List<Discoverer> discoverers = new ArrayList<>();
        for (String discovererName : discovererNames) {
            discoverers.add(new Discoverer(discovererName, elementId));
        }
        return discoverers;
    }

    private Resource getData(ClassLoader classLoader) {
        return new ClassPathResource("data/periodic_table.json", classLoader);
    }

    @Override
    public List<SimpleElementDto> getAllElements() {
        return this.elementDataRepository
                .findAll()
                .stream()
                .map(e -> new SimpleElementDto(e.getAtomicNumber(), e.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public List<ElementDataDto> getFilteredElements(Integer group, Integer period) {
        if (group == null && period == null) {
            throw new MissingIdentifierException("Group and Period not provided.");
        }
        List<ElementData> elements;
        if (group != null && period != null) {
            elements = this.elementDataRepository.findAllByGroupBlockAndPeriod(group, period);
        } else if (period == null) {
            elements = this.elementDataRepository.findAllByGroupBlock(group);
        } else {
            elements = this.elementDataRepository.findAllByPeriod(period);
        }
        List<ElementDataDto> result = new ArrayList<>();
        for (ElementData e: elements) {
            result.add(new ElementDataDto(e));
        }
        return result;
    }

    @Override
    public ElementDataDto getElementDetails(int atomicNumber) {
        return new ElementDataDto(getSingleElement(atomicNumber));
    }

    private ElementData getSingleElement(int atomicNumber) {
        return this.elementDataRepository.findByAtomicNumber(atomicNumber)
                .orElseThrow(() ->
                        new UnidentifiedElementException(
                                String.format("Unidentified element expected from atomic number %s", atomicNumber)));
    }

    @Transactional
    @Override
    public void updateElement(ElementDataDto elementDataDto) {
        ElementData element = getSingleElement(elementDataDto.getAtomicNumber());
        element.setElementDataFromDto(elementDataDto);
        int id = this.elementDataRepository.save(element).getId();
        discovererRepository.deleteAllByElementDataId(id);
        List<Discoverer> discoverers = mapDiscoverers(elementDataDto.getDiscoverers(), id);
        discovererRepository.saveAll(discoverers);
    }

}
