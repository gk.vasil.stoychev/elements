package com.vs.elements.features.element.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "discoverer")
public class Discoverer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    @Column(name = "element_data_id")
    private int elementDataId;

    public Discoverer() { }

    public Discoverer(String name, int elementDataId) {
        this.name = name;
        this.elementDataId = elementDataId;
    }
}
