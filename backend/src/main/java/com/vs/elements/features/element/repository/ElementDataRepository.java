package com.vs.elements.features.element.repository;

import com.vs.elements.features.element.model.ElementData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ElementDataRepository extends JpaRepository<ElementData, Integer> {

    Optional<ElementData> findByAtomicNumber(int atomicNumber);

    List<ElementData> findAllByGroupBlock(int groupBlock);

    List<ElementData> findAllByPeriod(int period);

    List<ElementData> findAllByGroupBlockAndPeriod(int groupBlock, int period);

    void removeByAtomicNumber(int atomicNumber);

}
