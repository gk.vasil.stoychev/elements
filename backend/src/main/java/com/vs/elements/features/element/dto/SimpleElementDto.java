package com.vs.elements.features.element.dto;

import lombok.Getter;

@Getter
public class SimpleElementDto {

    private final int atomicNumber;
    private final String name;

    public SimpleElementDto(int atomicNumber,
                            String name) {
        this.atomicNumber = atomicNumber;
        this.name = name;
    }
}
