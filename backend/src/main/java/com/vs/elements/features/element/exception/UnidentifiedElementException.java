package com.vs.elements.features.element.exception;

public class UnidentifiedElementException extends RuntimeException {

    public UnidentifiedElementException(String message) {
        super(message);
    }
}
