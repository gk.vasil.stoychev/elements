package com.vs.elements.features.element.repository;

import com.vs.elements.features.element.model.Discoverer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiscovererRepository extends JpaRepository<Discoverer, Integer> {

    void deleteAllByElementDataId(int elementDataId);
}
