package com.vs.elements.features.element.exception;

public class MissingIdentifierException extends RuntimeException {

    public MissingIdentifierException(String message) {
        super(message);
    }
}
