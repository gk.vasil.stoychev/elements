package com.vs.elements.features.element.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vs.elements.features.element.model.Discoverer;
import com.vs.elements.features.element.model.ElementData;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementDataDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final String NOT_AVAILABLE = "n/a";
	private static final String SPLIT = "( and | And |,|\\n|&)";
	private static final String UNKNOWN = "unknown";

	private int atomicNumber;
	private String name;
	private String alternativeName;
	private String atomicSymbol;
	private String appearance;
	private String[] discoverers;
	private String discoveryYear;
	private int group;
	private int period;

	@JsonCreator
	public ElementDataDto(@JsonProperty("atomic_number") int atomicNumber,
						  @JsonProperty("name") String name,
						  @JsonProperty("alternative_name") String alternativeName,
						  @JsonProperty("alternative_names") String alternativeNames,
						  @JsonProperty("symbol") String atomicSymbol,
						  @JsonProperty("appearance") String appearance,
						  @JsonProperty("discovery") String discoverers,
						  @JsonProperty("discovery_and_first_isolation") String discoverersAlternative,
						  @JsonProperty("group_block") String group,
						  @JsonProperty("period") int period) {
		this.atomicNumber = atomicNumber;
		this.name = name;
		this.alternativeName = prepareAlternativeName(alternativeName, alternativeNames);
		this.atomicSymbol = atomicSymbol;
		this.appearance = appearance;
		this.discoverers = prepareDiscoveryInformation(discoverers, discoverersAlternative).toArray(new String[]{});
		this.group = prepareGroup(group);
		this.period = period;
	}

	public ElementDataDto(ElementData elementData) {
		this.atomicNumber = elementData.getAtomicNumber();
		this.name = elementData.getName();
		this.alternativeName = elementData.getAlternativeName();
		this.atomicSymbol = elementData.getAtomicSymbol();
		this.appearance = elementData.getAppearance();
		mapDiscoverers(elementData.getDiscoverer());
		this.discoveryYear = elementData.getDiscoveryYear();
		this.group = elementData.getGroupBlock();
		this.period = elementData.getPeriod();
	}

	private void mapDiscoverers(List<Discoverer> discoverers) {
		this.discoverers = new String[discoverers.size()];
		for (int i = 0; i < this.discoverers.length; i++) {
			this.discoverers[i] = discoverers.get(i).getName();
		}
	}

	private String prepareAlternativeName(String alternativeName, String alternativeNames) {
		if (!alternativeName.equals(NOT_AVAILABLE)) {
			return alternativeName;
		} else if (!alternativeNames.equals(NOT_AVAILABLE)) {
			return alternativeNames;
		}

		return "none";
	}

	private List<String> prepareDiscoveryInformation(String discoverers, String discoverersAlternative) {
		List<String> result = new ArrayList<>(extractDiscoverers(discoverers));
		result.addAll(extractDiscoverers(discoverersAlternative));
		if (result.size() == 0 && StringUtils.hasLength(this.discoveryYear)) {
			this.discoveryYear = UNKNOWN;
		}

		return result.stream()
				.map(discoverer -> discoverer = discoverer.trim())
				.collect(Collectors.toList());
	}

	private List<String> extractDiscoverers(String discoverers) {
		if (!discoverers.equals(NOT_AVAILABLE)) {
			discoverers = extractAndClearDiscoveryYear(discoverers);
			return Arrays.asList(discoverers.split(SPLIT));
		}
		return new ArrayList<>();
	}

	private String extractAndClearDiscoveryYear(String data) {
		int yearBeginIndex = data.indexOf('(');
		int yearEndIndex = data.indexOf(')');
		if (yearBeginIndex == -1 || yearEndIndex == -1) {
			if (StringUtils.hasLength(this.discoveryYear)) {
				this.discoveryYear = UNKNOWN;
			}
			return data;
		} else {
			String date = data.substring(yearBeginIndex, yearEndIndex + 1);
			this.discoveryYear = date.replace("(", "").replace(")", "");
			return data.replace(date, "");
		}
	}

	private int prepareGroup(String group) {
		if (group.contains(NOT_AVAILABLE)) {
			return 0;
		}
		group = group.replace("group ", "");
		return Integer.parseInt(group.substring(0, 2).replace(",", "").trim());
	}
}
