package com.vs.elements.features.element.service;

import com.vs.elements.features.element.dto.ElementDataDto;
import com.vs.elements.features.element.dto.SimpleElementDto;

import java.io.IOException;
import java.util.List;

public interface ElementService {

    void loadDataToDb() throws IOException;

    List<SimpleElementDto> getAllElements();

    List<ElementDataDto> getFilteredElements(Integer group, Integer period) throws IOException;

    ElementDataDto getElementDetails(int atomicNumber);

    void updateElement(ElementDataDto elementDataDto) throws IOException;

}
