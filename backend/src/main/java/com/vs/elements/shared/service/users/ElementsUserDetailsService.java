package com.vs.elements.shared.service.users;

import com.vs.elements.security.model.UserLoginInformationDto;
import com.vs.elements.shared.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface ElementsUserDetailsService extends UserDetailsService {

    int createUser(UserDto userDto);

    UserLoginInformationDto authenticate(UserDto userDto);

}
