package com.vs.elements.shared.service.users;

import com.vs.elements.security.model.Authority;
import com.vs.elements.security.model.UserLoginInformationDto;
import com.vs.elements.security.model.UserRole;
import com.vs.elements.security.repository.AuthorityRepository;
import com.vs.elements.security.repository.ElementsUserRepository;
import com.vs.elements.security.service.jwt.JwtService;
import com.vs.elements.shared.dto.UserDto;
import com.vs.elements.shared.model.ElementsUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ElementsUserDetailsServiceImpl implements ElementsUserDetailsService {

    private final ElementsUserRepository elementsUserRepository;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ElementsUserDetailsServiceImpl(ElementsUserRepository elementsUserRepository,
                                          JwtService jwtService,
                                          AuthenticationManager authenticationManager,
                                          AuthorityRepository authorityRepository,
                                          PasswordEncoder passwordEncoder) {
        this.elementsUserRepository = elementsUserRepository;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ElementsUser elementsUser = elementsUserRepository.findElementsUserByUsername(username);

        return User.builder()
                .username(elementsUser.getUsername())
                .password(String.valueOf(elementsUser.getPassword()))
                .authorities(elementsUser.getAuthoritySet())
                .build();
    }

    @Override
    public int createUser(UserDto userDto) {
        ElementsUser elementsUser = new ElementsUser(userDto.getUsername(), passwordEncoder.encode(String.valueOf(userDto.getPassword())));
        int userId = elementsUserRepository.save(elementsUser).getId();
        saveAuthorities(elementsUser, userId);
        return userId;
    }

    private void saveAuthorities(ElementsUser elementsUser, int userId) {
        Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority(userId, UserRole.ROLE_USER.name()));
        if (elementsUser.getUsername().equals("admin")) {
            authorities.add(new Authority(userId, UserRole.ROLE_ADMIN.name()));
        }
        elementsUser.setAuthoritySet(authorities);

        authorityRepository.saveAll(authorities);
    }

    @Override
    public UserLoginInformationDto authenticate(UserDto userDto) {
        String username = userDto.getUsername();
        authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username, String.valueOf(userDto.getPassword())));

        String token = jwtService.createToken(username);
        ElementsUser user = elementsUserRepository.findElementsUserByUsername(username);

        return new UserLoginInformationDto(
                "Bearer " + token,
                username,
                user.getId(),
                user.getAuthoritySet().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ROLE_ADMIN.name())));
    }
}
