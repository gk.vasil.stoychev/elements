package com.vs.elements.api.element;

import com.vs.elements.features.element.dto.ElementDataDto;
import com.vs.elements.features.element.dto.SimpleElementDto;
import com.vs.elements.features.element.exception.MissingIdentifierException;
import com.vs.elements.features.element.exception.UnidentifiedElementException;
import com.vs.elements.features.element.service.ElementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/elements")
@CrossOrigin(origins = {"${angular.http.env}", "${angular.https.env}"})
public class ElementController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    ElementService elementService;

    @Autowired
    public ElementController(ElementService elementService) {
        this.elementService = elementService;
    }

    @GetMapping(path = "/")
    public ResponseEntity<List<SimpleElementDto>> getAllElements() {
        return ResponseEntity.ok(elementService.getAllElements());
    }

    @GetMapping(path = "/filtered")
    public ResponseEntity<List<ElementDataDto>> getFilteredElements(
            @RequestParam(value = "group", required = false) Integer group,
            @RequestParam(value = "period", required = false) Integer period) throws IOException {
        try {
            return ResponseEntity.ok(elementService.getFilteredElements(group, period));
        } catch (MissingIdentifierException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(path = "/details")
    public ResponseEntity<ElementDataDto> getElement(@RequestParam int atomicNumber) {
        try {
            return ResponseEntity.ok(elementService.getElementDetails(atomicNumber));
        } catch (UnidentifiedElementException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }

    @Secured("ROLE_ADMIN")
    @PostMapping(path = "/update")
    public ResponseEntity<Void> updateElement(
            @RequestBody ElementDataDto elementDataDto) throws IOException {
        try {
            elementService.updateElement(elementDataDto);
            LOGGER.warn(String.format("Element with Atomic Number %s has been updated.", elementDataDto.getAtomicNumber()));
            return ResponseEntity.ok().build();
        } catch (UnidentifiedElementException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }

}
