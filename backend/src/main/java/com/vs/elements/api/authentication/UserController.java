package com.vs.elements.api.authentication;

import com.vs.elements.shared.dto.UserDto;
import com.vs.elements.shared.service.users.ElementsUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = {"${angular.http.env}", "${angular.https.env}"})
public class UserController {

    private ElementsUserDetailsService elementsUserDetailsService;

    @Autowired
    private void setElementsUserDetailsService(ElementsUserDetailsService elementsUserDetailsService) {
        this.elementsUserDetailsService = elementsUserDetailsService;
    }

    @PostMapping(path = "/")
    public ResponseEntity<Integer> createUser(@RequestBody UserDto userDto) {
        int id = elementsUserDetailsService.createUser(userDto);
        return ResponseEntity.ok(id);
    }
}
