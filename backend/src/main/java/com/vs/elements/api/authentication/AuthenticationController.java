package com.vs.elements.api.authentication;

import com.vs.elements.shared.dto.UserDto;
import com.vs.elements.shared.service.users.ElementsUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/authentication")
@CrossOrigin(origins = {"${angular.http.env}", "${angular.https.env}"})
public class AuthenticationController {

    private ElementsUserDetailsService elementsUserDetailsService;

    @Autowired
    private void setElementsUserDetailsService(ElementsUserDetailsService elementsUserDetailsService) {
        this.elementsUserDetailsService = elementsUserDetailsService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(@RequestBody UserDto userDto) {
        try {
            return ResponseEntity.ok(elementsUserDetailsService.authenticate(userDto));
        } catch (Exception e) {
            //log exception
            return ResponseEntity.badRequest().build();
        }
    }
}
