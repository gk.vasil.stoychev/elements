package com.vs.elements;

import com.vs.elements.features.element.service.ElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.IOException;

@SpringBootApplication
public class ElementsApplication {

    private ElementService elementService;

    @Autowired
    private void setElementService(ElementService elementService) {
        this.elementService = elementService;
    };

    public static void main(String[] args) {
        SpringApplication.run(ElementsApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void setupDatabase() throws IOException {
        elementService.loadDataToDb();
    }

}
