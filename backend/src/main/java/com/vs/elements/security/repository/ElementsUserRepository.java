package com.vs.elements.security.repository;

import com.vs.elements.shared.model.ElementsUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ElementsUserRepository extends JpaRepository<ElementsUser, Integer> {

    ElementsUser findElementsUserByUsername(String username);

}
