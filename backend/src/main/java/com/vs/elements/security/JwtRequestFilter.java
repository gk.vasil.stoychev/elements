package com.vs.elements.security;

import com.vs.elements.security.service.jwt.JwtService;
import com.vs.elements.shared.service.users.ElementsUserDetailsService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";

    private ElementsUserDetailsService elementsUserDetailsService;
    private JwtService jwtService;

    @Autowired
    private void setElementsUserDetailsService(ElementsUserDetailsService elementsUserDetailsService) {
        this.elementsUserDetailsService = elementsUserDetailsService;
    }

    @Autowired
    private void setJwtUtil(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        String jwt;
        String username;
        String token = httpServletRequest.getHeader(AUTHORIZATION);

        if (Strings.isNotEmpty(token) && token.startsWith(BEARER)) {
            jwt = token.replace(BEARER, "");
            username = jwtService.extractUsername(jwt);
        } else {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        if (Strings.isNotEmpty(username) && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = elementsUserDetailsService.loadUserByUsername(username);
            if (jwtService.validateToken(jwt, userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
