package com.vs.elements.security.model;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
