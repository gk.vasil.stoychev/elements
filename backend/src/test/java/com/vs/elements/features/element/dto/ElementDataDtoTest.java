package com.vs.elements.features.element.dto;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ElementDataDtoTest {

    @ParameterizedTest
    @MethodSource("prepareElements")
    void elementDataTestObjectConstruction_valid_valid(ElementTestData testData) {
        ElementDataDto dto = new ElementDataDto(testData.atomicNumber,
                testData.name,
                testData.alternativeName,
                testData.alternativeNames,
                testData.atomicSymbol,
                testData.appearance,
                testData.discoverers,
                testData.discoverersAlternative,
                testData.group,
                testData.period);

        assertEquals(testData.atomicNumber, dto.getAtomicNumber());
        assertEquals(testData.name, dto.getName());
        assertEquals(testData.alternativeName.equals("n/a") ? testData.alternativeNames : testData.alternativeName, dto.getAlternativeName());
        assertEquals(testData.atomicSymbol, dto.getAtomicSymbol());
        assertEquals(testData.appearance, dto.getAppearance());
        assertEquals(testData.expectedDiscoverersNumber, dto.getDiscoverers().length);
        for (int i = 0; i < testData.expectedDiscoverersNumber; i++) {
            assertEquals(testData.expectedNames[i], dto.getDiscoverers()[i]);
        }
        assertEquals(testData.expectedYearOfDiscovery, dto.getDiscoveryYear());
        assertEquals(testData.expectedGroup, dto.getGroup());
        assertEquals(testData.period, dto.getPeriod());
    }

    private static Stream<ElementTestData> prepareElements() {
        return Stream.of(
                new ElementTestData(1,
                        "First",
                        "Alternative",
                        "n/a",
                        "H",
                        "colorless gas",
                        "William Thomas Brande (1821)",
                        "n/a",
                        "group 1 (alkali metals), s-block",
                        1,
                        new String[] { "William Thomas Brande" },
                        1,
                        "1821",
                        1),
                new ElementTestData(100,
                        "Second",
                        "n/a",
                        "Second Row Alt Name",
                        "Z",
                        "silver white",
                        "n/a",
                        "William Hyde Wollaston & Andand Ex And Another (1804)",
                        "group 11, d-block",
                        5,
                        new String[] { "William Hyde Wollaston", "Andand Ex", "Another" },
                        3,
                        "1804",
                        11)
                );
    }

    private static class ElementTestData {
        int atomicNumber;
        String name;
        String alternativeName;
        String alternativeNames;
        String atomicSymbol;
        String appearance;
        String discoverers;
        String discoverersAlternative;
        String group;
        int period;
        String[] expectedNames;
        int expectedDiscoverersNumber;
        String expectedYearOfDiscovery;
        int expectedGroup;

        public ElementTestData(int atomicNumber,
                           String name,
                           String alternativeName,
                           String alternativeNames,
                           String atomicSymbol,
                           String appearance,
                           String discoverers,
                           String discoverersAlternative,
                           String group,
                           int period,
                           String[] expectedNames,
                           int expectedDiscoverersNumber,
                           String expectedYearOfDiscovery,
                           int expectedGroup
        ) {
            this.atomicNumber = atomicNumber;
            this.name = name;
            this.alternativeName = alternativeName;
            this.alternativeNames = alternativeNames;
            this.atomicSymbol = atomicSymbol;
            this.appearance = appearance;
            this.discoverers = discoverers;
            this.discoverersAlternative = discoverersAlternative;
            this.group = group;
            this.period = period;
            this.expectedNames = expectedNames;
            this.expectedDiscoverersNumber = expectedDiscoverersNumber;
            this.expectedYearOfDiscovery = expectedYearOfDiscovery;
            this.expectedGroup = expectedGroup;
        }
    }
}