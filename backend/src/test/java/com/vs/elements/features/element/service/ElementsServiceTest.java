package com.vs.elements.features.element.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vs.elements.features.element.dto.ElementDataDto;
import com.vs.elements.features.element.dto.SimpleElementDto;
import com.vs.elements.features.element.exception.MissingIdentifierException;
import com.vs.elements.features.element.exception.UnidentifiedElementException;
import com.vs.elements.features.element.model.Discoverer;
import com.vs.elements.features.element.model.ElementData;
import com.vs.elements.features.element.repository.DiscovererRepository;
import com.vs.elements.features.element.repository.ElementDataRepository;
import com.vs.elements.shared.builder.ElementDataBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.vs.elements.shared.mocks.ElementDataMocks.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ElementsServiceTest {

    private final ObjectMapper objectMapperMock = mock(ObjectMapper.class);
    private final ElementDataRepository elementDataRepositoryMock = mock(ElementDataRepository.class);
    private final DiscovererRepository discovererRepositoryMock = mock(DiscovererRepository.class);
    private final ElementService elementService =
            new ElementServiceImpl(objectMapperMock, elementDataRepositoryMock, discovererRepositoryMock);

    @Test
    void loadDataToDb_existingElementsInDb_doesNotAddElements() throws IOException {
        when(elementDataRepositoryMock.count()).thenReturn(120L);

        elementService.loadDataToDb();

        verify(elementDataRepositoryMock, times(1)).count();
        verifyNoInteractions(objectMapperMock);
        verify(elementDataRepositoryMock, times(0)).save(any());
        verify(discovererRepositoryMock, times(0)).saveAll(anyCollection());
    }

    @Test
    void loadDataToDb_noElementsInDb_addsElements() throws IOException {
        ElementData[] elementData = prepareElementData();
        when(elementDataRepositoryMock.count()).thenReturn(0L);
        when(objectMapperMock.readValue(any(InputStream.class), any(ElementDataDto[].class.getClass())))
                .thenReturn(prepareElementDataDto(elementData));
        when(elementDataRepositoryMock.save(any(ElementData.class))).thenReturn(elementData[0]);

        elementService.loadDataToDb();

        verify(elementDataRepositoryMock, times(1)).count();
        verify(objectMapperMock, times(1))
                .readValue(any(InputStream.class), any(ElementDataDto[].class.getClass()));
        verify(elementDataRepositoryMock, times(elementData.length)).save(any(ElementData.class));
        verify(discovererRepositoryMock, times(elementData.length)).saveAll(anyCollection());
    }

    @Test
    void getAllElements_presentData_getsAllSimpleElements() {
        when(elementDataRepositoryMock.findAll()).thenReturn(Arrays.asList(prepareElementData()));

        List<SimpleElementDto> simpleElements = elementService.getAllElements();

        assertEquals(3, simpleElements.size());
        assertEquals(FIRST_ELEMENT, simpleElements.get(0).getName());
        assertEquals(2, simpleElements.get(1).getAtomicNumber());
        assertEquals(THIRD_ELEMENT, simpleElements.get(2).getName());
        verify(elementDataRepositoryMock, times(1)).findAll();
    }

    @Test
    void getFilteredElements_groupAndPeriodMissing_throws() {
        assertThrows(MissingIdentifierException.class, () -> elementService.getFilteredElements(null, null));
    }

    @Test
    void getFilteredElements_groupMissing_filterByPeriod() throws IOException {
        ElementData[] elements = prepareElementData();
        when(elementDataRepositoryMock.findAllByPeriod(1)).thenReturn(List.of(elements[0], elements[2]));

        List<ElementDataDto> elementsData = elementService.getFilteredElements(null, 1);

        assertEquals(2, elementsData.size());
        assertEquals(1, elementsData.get(0).getPeriod());
        assertEquals(1, elementsData.get(1).getPeriod());
        assertEquals(FIRST_ELEMENT, elementsData.get(0).getName());
        assertEquals(THIRD_ELEMENT, elementsData.get(1).getName());
        verify(elementDataRepositoryMock, times(1)).findAllByPeriod(1);
    }

    @Test
    void getFilteredElements_periodMissing_filterByGroup() throws IOException {
        ElementData[] elements = prepareElementData();
        when(elementDataRepositoryMock.findAllByGroupBlock(2)).thenReturn(List.of(elements[1]));

        List<ElementDataDto> elementsData = elementService.getFilteredElements(2, null);

        assertEquals(1, elementsData.size());
        assertEquals(2, elementsData.get(0).getGroup());
        assertEquals(SECOND_ELEMENT, elementsData.get(0).getName());
        verify(elementDataRepositoryMock, times(1)).findAllByGroupBlock(2);
    }

    @Test
    void getFilteredElements_filterByBoth_singleItem() throws IOException {
        ElementData[] elements = prepareElementData();
        when(elementDataRepositoryMock.findAllByGroupBlockAndPeriod(1, 1)).thenReturn(List.of(elements[0]));

        List<ElementDataDto> elementsData = elementService.getFilteredElements(1, 1);

        assertEquals(1, elementsData.size());
        assertEquals(1, elementsData.get(0).getGroup());
        assertEquals(1, elementsData.get(0).getPeriod());
        assertEquals(FIRST_ELEMENT, elementsData.get(0).getName());
        verify(elementDataRepositoryMock, times(1)).findAllByGroupBlockAndPeriod(1, 1);
    }

    @ParameterizedTest
    @MethodSource("prepareGroupPeriod")
    void getFilteredElements_invalidItem_emptyList(GroupAndPeriod groupAndPeriod) throws IOException {
        when(elementDataRepositoryMock.findAllByGroupBlockAndPeriod(anyInt(), anyInt())).thenReturn(List.of());
        when(elementDataRepositoryMock.findAllByGroupBlock(anyInt())).thenReturn(List.of());
        when(elementDataRepositoryMock.findAllByPeriod(anyInt())).thenReturn(List.of());

        List<ElementDataDto> elementsData = elementService.getFilteredElements(groupAndPeriod.group, groupAndPeriod.period);

        assertEquals(0, elementsData.size());
    }

    private static Stream<GroupAndPeriod> prepareGroupPeriod() {
        return Stream.of(
                new GroupAndPeriod(100, 100),
                new GroupAndPeriod(0, 0),
                new GroupAndPeriod(null, 100),
                new GroupAndPeriod(100, null)
        );
    }

    private static class GroupAndPeriod {
        Integer group;
        Integer period;

        public GroupAndPeriod(Integer group, Integer period) {
            this.group = group;
            this.period = period;
        }
    }

    @Test
    void getElementDetails_nonExistingAtomicNumber_throws() {
        when(elementDataRepositoryMock.findByAtomicNumber(anyInt())).thenThrow(UnidentifiedElementException.class);

        assertThrows(UnidentifiedElementException.class, () -> elementService.getElementDetails(100));
    }

    @Test
    void getElementDetails_existingAtomicNumber_getsElement() {
        when(elementDataRepositoryMock.findByAtomicNumber(1)).thenReturn(Optional.of(prepareElementData()[0]));

        ElementDataDto elementData = elementService.getElementDetails(1);

        assertEquals(FIRST_ELEMENT, elementData.getName());
        assertEquals("Person Man", elementData.getDiscoverers()[0]);
        verify(elementDataRepositoryMock, times(1)).findByAtomicNumber(1);
    }

    @Test
    void updateElement_nonExistingAtomicNumber_throws() {
        ElementDataDto dto = prepareElementDataDto(prepareElementData())[0];
        when(elementDataRepositoryMock.findByAtomicNumber(dto.getAtomicNumber())).thenThrow(UnidentifiedElementException.class);

        assertThrows(UnidentifiedElementException.class, () -> elementService.updateElement(dto));

        verify(elementDataRepositoryMock, times(1)).findByAtomicNumber(dto.getAtomicNumber());
        verify(elementDataRepositoryMock, times(0)).save(any(ElementData.class));
        verify(discovererRepositoryMock, times(0)).deleteAllByElementDataId(anyInt());
        verify(discovererRepositoryMock, times(0)).save(any(Discoverer.class));
    }

    @Test
    void updateElement_validData_savesElement() throws IOException {
        ElementDataDto dto = prepareElementDataDto(prepareElementData())[0];
        ElementData element = prepareElementData()[0];
        when(elementDataRepositoryMock.findByAtomicNumber(dto.getAtomicNumber())).thenReturn(Optional.of(element));
        when(elementDataRepositoryMock.save(any(ElementData.class))).thenReturn(element);
        when(discovererRepositoryMock.saveAll(anyCollection())).thenReturn(element.getDiscoverer());

        elementService.updateElement(dto);

        verify(elementDataRepositoryMock, times(1)).findByAtomicNumber(dto.getAtomicNumber());
        verify(elementDataRepositoryMock, times(1)).save(any(ElementData.class));
        verify(discovererRepositoryMock, times(1)).deleteAllByElementDataId(anyInt());
        verify(discovererRepositoryMock, times(1)).saveAll(anyIterable());
    }

}
