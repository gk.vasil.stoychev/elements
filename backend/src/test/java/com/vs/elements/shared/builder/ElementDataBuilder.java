package com.vs.elements.shared.builder;

import com.vs.elements.features.element.model.Discoverer;
import com.vs.elements.features.element.model.ElementData;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ElementDataBuilder {

    private final int id;
    private final int atomicNumber;
    private String name;
    private String alternativeName;
    private String atomicSymbol;
    private String appearance;
    private List<Discoverer> discoverers;
    private String discoveryYear;
    private int group;
    private int period;

    public ElementDataBuilder(int id, int atomicNumber) {
        this.id = id;
        this.atomicNumber = atomicNumber;
    }

    public ElementDataBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ElementDataBuilder withAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
        return this;
    }

    public ElementDataBuilder withAtomicSymbol(String atomicSymbol) {
        this.atomicSymbol = atomicSymbol;
        return this;
    }

    public ElementDataBuilder withAppearance(String appearance) {
        this.appearance = appearance;
        return this;
    }

    public ElementDataBuilder withDiscoverers(List<Discoverer> discoverers) {
        this.discoverers = discoverers;
        return this;
    }

    public ElementDataBuilder withDiscoveryYear(String discoveryYear) {
        this.discoveryYear = discoveryYear;
        return this;
    }

    public ElementDataBuilder withGroup(int group) {
        this.group = group;
        return this;
    }

    public ElementDataBuilder withPeriod(int period) {
        this.period = period;
        return this;
    }

    public ElementData build() {
        return new ElementData(
                this.id,
                this.atomicNumber,
                this.name,
                this.alternativeName,
                this.atomicSymbol,
                this.appearance,
                this.discoverers,
                this.discoveryYear,
                this.group,
                this.period
        );
    }
}
