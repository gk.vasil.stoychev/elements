package com.vs.elements.shared.mocks;

import com.vs.elements.features.element.dto.ElementDataDto;
import com.vs.elements.features.element.model.Discoverer;
import com.vs.elements.features.element.model.ElementData;
import com.vs.elements.shared.builder.ElementDataBuilder;

import java.util.List;

public class ElementDataMocks {

    public static final String FIRST_ELEMENT = "first";
    public static final String SECOND_ELEMENT = "second";
    public static final String THIRD_ELEMENT = "third";

    public static ElementData[] prepareElementData() {
        ElementData first = new ElementDataBuilder(1, 1)
                .withName(FIRST_ELEMENT)
                .withGroup(1)
                .withPeriod(1)
                .withDiscoverers(List.of(new Discoverer("Person Man", 1)))
                .build();

        ElementData second = new ElementDataBuilder(2, 2)
                .withName(SECOND_ELEMENT)
                .withGroup(2)
                .withPeriod(2)
                .withDiscoverers(List.of(new Discoverer("Guy Buddy", 2), new Discoverer("Hello Kitty", 2)))
                .build();

        ElementData third = new ElementDataBuilder(3, 3)
                .withName(THIRD_ELEMENT)
                .withGroup(3)
                .withPeriod(1)
                .withDiscoverers(List.of())
                .build();

        return new ElementData[] { first, second, third };
    }

    public static ElementDataDto[] prepareElementDataDto(ElementData[] data) {
        return new ElementDataDto[] {
                new ElementDataDto(data[0]),
                new ElementDataDto(data[1]),
                new ElementDataDto(data[2])
        };
    }
}
