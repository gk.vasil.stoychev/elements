package com.vs.elements.api.element;

import com.vs.elements.features.element.dto.ElementDataDto;
import com.vs.elements.features.element.dto.SimpleElementDto;
import com.vs.elements.features.element.exception.MissingIdentifierException;
import com.vs.elements.features.element.exception.UnidentifiedElementException;
import com.vs.elements.features.element.service.ElementService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.vs.elements.shared.mocks.ElementDataMocks.prepareElementData;
import static com.vs.elements.shared.mocks.ElementDataMocks.prepareElementDataDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ElementControllerTest {

    private final ElementService elementServiceMock = mock(ElementService.class);
    private final ElementController elementController = new ElementController(elementServiceMock);

    @Test
    void getAllElements_presentData_ok() {
        when(elementServiceMock.getAllElements()).thenReturn(
                List.of(prepareElementDataDto(prepareElementData()))
                .stream()
                .map(e -> new SimpleElementDto(e.getAtomicNumber(), e.getName()))
                .collect(Collectors.toList()));

        ResponseEntity<List<SimpleElementDto>> responseEntity = elementController.getAllElements();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(3, Objects.requireNonNull(responseEntity.getBody()).size());
        assertEquals(1, responseEntity.getBody().get(0).getAtomicNumber());
        verify(elementServiceMock, times(1)).getAllElements();
    }

    @SneakyThrows
    @Test
    void getFilteredElements_groupAndPeriodMissing_badRequest() {
        when(elementServiceMock.getFilteredElements(null, null)).thenThrow(MissingIdentifierException.class);

        ResponseEntity<?> responseEntity = elementController.getFilteredElements(null, null);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        verify(elementServiceMock, times(1)).getFilteredElements(any(), any());
    }

    @Test
    void getFilteredElements_filterByBoth_ok() throws IOException {
        ElementDataDto[] elements = prepareElementDataDto(prepareElementData());
        when(elementServiceMock.getFilteredElements(1, 1))
                .thenReturn(List.of(elements[0]));

        ResponseEntity<List<ElementDataDto>> responseEntity = elementController.getFilteredElements(1, 1);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(elements[0], Objects.requireNonNull(responseEntity.getBody()).get(0));
        verify(elementServiceMock, times(1)).getFilteredElements(any(), any());
    }

    @Test
    void getElement_nonExistingAtomicNumber_badRequest() {
        when(elementServiceMock.getElementDetails(100)).thenThrow(UnidentifiedElementException.class);

        ResponseEntity<?> responseEntity = elementController.getElement(100);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        verify(elementServiceMock, times(1)).getElementDetails(anyInt());
    }

    @Test
    void getElement_existingAtomicNumber_ok() {
        ElementDataDto elementDataDto = prepareElementDataDto(prepareElementData())[0];
        when(elementServiceMock.getElementDetails(1))
                .thenReturn(elementDataDto);

        ResponseEntity<ElementDataDto> responseEntity = elementController.getElement(1);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(elementDataDto, responseEntity.getBody());
        verify(elementServiceMock, times(1)).getElementDetails(anyInt());
    }

    @Test
    void updateElement_nonExistingAtomicNumber_savesElement() throws IOException {
        ElementDataDto elementDataDto = prepareElementDataDto(prepareElementData())[0];
        doThrow(UnidentifiedElementException.class).when(elementServiceMock).updateElement(elementDataDto);

        ResponseEntity<Void> responseEntity = elementController.updateElement(elementDataDto);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        verify(elementServiceMock, times(1)).updateElement(any(ElementDataDto.class));
    }

    @Test
    void updateElement_validData_savesElement() throws IOException {
        ElementDataDto elementDataDto = prepareElementDataDto(prepareElementData())[0];

        ResponseEntity<Void> responseEntity = elementController.updateElement(elementDataDto);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        verify(elementServiceMock, times(1)).updateElement(any(ElementDataDto.class));
    }
}
